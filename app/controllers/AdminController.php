<?php

class AdminController extends \BaseController {



	/**
	 * Display a listing of the resource.
	 * GET /admin
	 *
	 * @return Response
	 */
	public function index()
	{
        $recipes = DB::table('recipes')->limit(5);
       return View::make('admin/dashboard', compact('recipes'));
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /admin/create
	 *
	 * @return Response
	 */


	/**
	 * Store a newly created resource in storage.
	 * POST /admin
	 *
	 * @return Response
	 */
//	public function login()
//	{
//        return View::make('admin/login');
//
//	}

	/**
	 * Display the specified resource.
	 * GET /admin/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function logout()
	{
		Auth::logout();
        Redirect::to('admin.login');
	}

//    public function dashboard()
//    {
//        return View::make('admin.dashboard');
//    }




}
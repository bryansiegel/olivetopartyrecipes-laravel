<?php

class RecipeInstructionController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /recipeinstruction
	 *
	 * @return Response
	 */
	public function index()
	{
		return "hello";
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /recipeinstruction/create
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('recipes.instructions.create');
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /recipeinstruction
	 *
	 * @return Response
	 */
	public function store()
	{
		$instruction = Input::get('instruction');
        $ingredient = Input::get('ingredient');
        $id = Input::get('id');

        //ingredient
        if(!empty($ingredient)){
            Recipe::find($id)->ingredients()->insert([
                'ingredient' => $ingredient,
                'recipe_id' => $id
            ]);
        }

        //instruction
        if(!empty($instruction))
        {
            Recipe::find($id)->instructions()->insert([
                'instruction' => $instruction,
                'recipe_id' => $id
            ]);
        }


        return Redirect::action('recipeinstructions.show', array($id));

	}

	/**
	 * Display the specified resource.
	 * GET /recipeinstruction/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        $recipes = Recipe::find($id);


        $input = Input::all();
		return View::make('recipes.instructions.show', compact('recipes'));
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /recipeinstruction/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $recipe = Instruction::find($id);

        return View::make('recipes.instructions.edit', compact('recipe'));
	}


	/**
	 * Update the specified resource in storage.
	 * PUT /recipeinstruction/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{


        $input = Input::all();
        $instruction = Instruction::find($id);
        $instruction->update($input);

        //return to the id of the recipe not instruction
        return Redirect::action('recipeinstructions.show', $instruction->recipe_id);


	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /recipeinstruction/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        $instruction = Instruction::find($id);
        Instruction::destroy($id);
        return Redirect::action('recipeinstructions.show', $instruction->recipe_id);
	}

}
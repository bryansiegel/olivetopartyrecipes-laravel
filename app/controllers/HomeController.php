<?php

class HomeController extends BaseController {


	public function index()
    {
        $recipes = Recipe::all();
        return View::make('home.index', compact('recipes'));
    }

}

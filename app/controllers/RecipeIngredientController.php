<?php

class RecipeIngredientController extends \BaseController {


	/**
	 * Store a newly created resource in storage.
	 * POST /recipeingredient
	 *
	 * @return Response
	 */
    public function store()
    {
        $ingredient = Input::get('ingredient');
        $id = Input::get('id');

        //ingredient
        if(!empty($ingredient)){
            Recipe::find($id)->ingredients()->insert([
                'ingredient' => $ingredient,
                'recipe_id' => $id
            ]);
        }

        return Redirect::action('recipeingredients.show', array($id));

    }

	/**
	 * Display the specified resource.
	 * GET /recipeingredient/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        $recipes = Recipe::find($id);


        $input = Input::all();
        return View::make('recipes.ingredients.show', compact('recipes'));
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /recipeingredient/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $recipe = Ingredient::find($id);

        return View::make('recipes.ingredients.edit', compact('recipe'));
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /recipeingredient/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
        $input = Input::all();
        $ingredient = Ingredient::find($id);
        $ingredient->update($input);

        //return to the id of the recipe not instruction
        return Redirect::action('recipeinstructions.show', $ingredient->recipe_id);
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /recipeingredient/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        $ingredient = Ingredient::find($id);
        Ingredient::destroy($id);

        return Redirect::action('recipeinstructions.show', $ingredient->recipe_id);
	}

}
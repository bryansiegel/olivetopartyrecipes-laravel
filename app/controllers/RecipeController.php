<?php

class RecipeController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /recipe
	 *
	 * @return Response
	 */
	public function index()
	{
		$recipes = Recipe::all();

        return View::make('recipes.index', compact('recipes'));
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /recipe/create
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('recipes.create');
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /recipe
	 *
	 * @return Response
	 */
	public function store()
	{

        //file upload
        $file = Input::file('img');
//        local
        $destinationPath = 'public/img/';
//        depoloyment
//        $destinationPath = 'img/';

        $filename = $file->getClientOriginalName();
        Input::file('img')->move($destinationPath, $filename);



        $input = [
            'title' => Input::get('title'),
            'pairs' => Input::get('pairs'),
            'description' => Input::get('description'),
            'img' => $filename
        ];


        $validation = Validator::make($input, Recipe::$rules);

        if ($validation->passes())
        {
            Recipe::create($input);

            return Redirect::route('recipes.index');
        }

        return Redirect::route('recipes.create')
            ->withInput()
            ->withErrors($validation)
            ->with('message', 'There were validation errors.');
	}

	/**
	 * Display the specified resource.
	 * GET /recipe/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$recipes = Recipe::find($id);

        return View::make('recipes.show', compact('recipes'));
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /recipe/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $recipe = Recipe::find($id);

        return View::make('recipes.edit', compact('recipe'));
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /recipe/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
        $input = Input::all();
        $validation = Validator::make($input, Recipe::$rules);
        if ($validation->passes())
        {
            $recipe = Recipe::find($id);
            $recipe->update($input);
            return Redirect::route('recipes.show', $id);
        }
        return Redirect::route('recipes.edit', $id)
            ->withInput()
            ->withErrors($validation)
            ->with('message', 'There were validation errors.');
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /recipe/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Recipe::destroy($id);
       return Redirect::to('recipes');
	}

}
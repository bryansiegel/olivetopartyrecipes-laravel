<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStepToRecipesInstructionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('recipes_instructions', function(Blueprint $table)
		{
			$table->tinyInteger('step_instructions');
            $table->tinyInteger('step_ingredients');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('recipes_instructions', function(Blueprint $table)
		{
			//
		});
	}

}

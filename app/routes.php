<?php

Route::group(array('before' => 'auth'), function()
{
//recipes
    Route::resource('/recipes', 'RecipeController');
//users
    Route::resource('/users', 'UserController');
//recipe instructions
    Route::resource('recipeinstructions', 'RecipeInstructionController');
//recipe ingredients
    Route::resource('recipeingredients', 'RecipeIngredientController');
});


//login
Route::get('admin/login',function(){
    return View::make('admin/login');
});

Route::post('admin/login', function(){
   $userdata = array(
       'username' => Input::get('username'),
       'password' => Input::get('password'),
   );
    if(Auth::attempt($userdata)){
        return Redirect::to('/admin');
    }
    else {
        return View::make('admin/login')
            ->with('message' ,'Incorrect Login Please Try Again')
            ;
    }

});

//logout
Route::get('admin/logout', array('before' => 'auth', function(){
    Auth::logout();
    return Redirect::to('admin/login');
}));

//dashboard
Route::get('admin', array('before' => 'auth',function(){
    $recipes = DB::table('recipes')->limit(5)->get();
    return View::make('admin/dashboard', compact('recipes'));
}));

//home
Route::get('/', function(){
    $recipes = Recipe::all();
    return View::make('home.index', compact('recipes'));
});

//test

Route::get('/test', function(){


});


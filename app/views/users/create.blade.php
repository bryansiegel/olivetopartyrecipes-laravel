@extends('layouts.admin.dashboard')

@section('content')
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Users > Create</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>


        {{ Form::open(array('route' => 'users.store')) }}
        <div class="form-group">
            {{ Form::label('username', 'Username:') }}
            {{ Form::text('username',null, array('class' => 'form-control', 'placeholder' => 'Username')) }}
        </div>

        <div class="form-group">
            {{ Form::label('email', 'Email:') }}
            {{ Form::text('email', null, array('class' => 'form-control', 'placeholder' => 'Email')) }}
        </div>

        <div class="form-group">
        </div>


        {{ Form::submit('Submit', null, array('class' => 'btn btn-default')) }}


        {{ Form::close() }}


    </div>

@stop
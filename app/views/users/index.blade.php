@extends('layouts.admin.dashboard')

@section('content')
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Users</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        {{ link_to_route('users.create', 'Add A New User') }}

        <table class="table table-hover">
            <thead>
            <tr>
                <th>Username</th>
                <th>Email</th>
                {{--<th>Description</th>--}}
            </tr>
            </thead>
            <tbody>

            @foreach($users as $user)
                <tr>
                    <td>{{ $user->username }}</td>
                    <td>{{ $user->email }}</td>
                    {{--<td>{{ link_to_route('users.show', 'Show', array($user->id)) }}</td>--}}
                    <td>{{ link_to_route('users.edit', 'Edit', array($user->id)) }}</td>
                    {{--<td>{{ Form::open(array('method' => 'DELETE', 'route' => array('users.destroy', $user->id))) }}--}}
                        {{--{{ Form::submit('Delete') }}--}}
                        {{--{{ Form::close() }}--}}
                    {{--</td>--}}
                </tr>
            @endforeach

            </tbody>
        </table>
    </div>

@stop
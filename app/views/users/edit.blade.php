@extends('layouts.admin.dashboard')

@section('content')
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Users > Edit</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        {{ Form::model($user, array('method' => 'PATCH', 'route' =>
         array('users.update', $user->id))) }}
        <div class="form-group">
            {{ Form::label('username', 'Username:') }}
            {{ Form::text('username', null,array('class' => 'form-control')) }}
        </div>
        <div class="form-group">
            {{ Form::label('email', 'Email:') }}
            {{ Form::text('email', null, array('class' => 'form-control')) }}
        </div>
        <div class="form-group">

        </div>
        {{ Form::submit('Update', array('class' => 'btn btn-info')) }}
        {{ link_to_route('users.index', 'Cancel', $user->
id, array('class' => 'btn')) }}

        {{ Form::close() }}
    </div>

@stop


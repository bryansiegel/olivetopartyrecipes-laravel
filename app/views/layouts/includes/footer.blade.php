<footer class="footer">
<div class="container">
    <p class="text-muted" style="margin-top:15px;">
        @Cibaria International <?php echo Date("Y"); ?>
        <span style="float:right;"><a href="/admin">Login</a></span>
    </p>

</div>

<!-- Core JavaScript Files -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="http://netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>

<!-- custom js -->
{{ HTML::script('js/grayscale.js') }}
</footer>
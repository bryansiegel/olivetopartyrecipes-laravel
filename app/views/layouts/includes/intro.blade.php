<section class="intro">
    <div class="intro-body">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2" style="background: rgba(300, 54, 54, 0.5);">
                    <h1 class="brand-heading" style=";;">O'Live To Party Recipes</h1>

                    <p class="intro-text" style="background-color:purple;">Let's Get This Party On.</p>

                    <div class="page-scroll">
                        <a href="#about" class="btn btn-circle">
                            <i class="fa fa-angle-double-down animated"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
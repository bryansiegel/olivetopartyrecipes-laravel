<!--recipe1-->
<!-- Modal -->
<div class="modal fade" id="recipe1" tabindex="-1" role="dialog" aria-labelledby="1Label" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="background-image: url('img/recipe1.png');background-repeat: no-repeat;">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="recipe1Label" style="color:#000000; text-align: center;">Linguini with Aglio e Olio</h4>
            </div>
            <div class="modal-body" style="color:black;">
                <h4>Ingredients</h4>
                <ul class="list-group">
                    <li class="list-group-item">1 pound (1 package) dried linguini (also good with spaghetti or fetttuccini)</li>
                    <li class="list-group-item">1/2 cup Tuscan Herb Olive Oil or Sweet Basil Infused</li>
                    <li class="list-group-item">2-6 cloves garlic, sliced paper thin, (or whole cloves, crushed)</li>
                    <li class="list-group-item">2 red chili peppers, chopped (or 1/2 tsp. red pepper flakes or more to taste)</li>
                    <li class="list-group-item">1/4 cup reserved pasta water</li>
                    <li class="list-group-item">1 cup freshly-grated Parmesan cheese</li>
                    <li class="list-group-item">A handful (1/4 cup) Fresh flat-leaf Italian parsley (you can substitute cilantro but don't use dried parsley)</li>
                </ul>
                <h4>Instructions</h4>
                <ol>
                    <li>
                        NOTE: The cooking of the pasta and the preparation of the sauce take about the same time.
                    </li>
                    <li>
                        Put a large pot of cold water on to boil. (4 quarts or liters per pound of pasta) When the water is at a rolling boil, add a tablespoon or 2 of salt and add the pasta. Stir to prevent sticking. (no oil needed) Cook linguini according to package directions but test for doneness at least a minute before the end of the time on the package. You want it slightly firm (al dente). Keep in mind that it will continue to cook in the pan once it has been combined with the oil.
                    </li>
                    <li>
                        Reserve 1/4 cup or so of the starchy pasta water for the sauce.
                    </li>
                    <li>
                        While the pasta is cooking...
                    </li>
                    <li>
                        In a medium saute or saucepan (cold) add the olive oil and the garlic. Turn the heat to medium and slowly "toast" the garlic. DO NOT let it burn or it will be bitter! As soon as the oil starts to bubble around the garlic, turn the heat down to medium low. This only takes about 5 minutes so keep an eye on it.
                    </li>
                    <li>
                        OPTIONAL: Add 2 red chili peppers, chopped. (If using red pepper flakes, add them later with the herbs and cheese.) Cook the garlic and chillies, stirring occasionally, until fragrant. When the garlic is ready, the edges should be a beautiful golden brown color. If using large, crushed garlic pieces, remove them before adding the pasta. The oil will be infused with the garlic flavor.
                    </li>
                    <li>
                        Add the drained pasta directly to the hot saucepan. (or add the oil to your pasta bowl as shown in the video) Mix well to coat the strands and allow to heat through for about a minute or so.
                    </li>
                    <li>
                        Add in the reserved pasta water as needed to loosen the sauce. If you want more heat, add a few fresh chillies.
                    </li>
                    <li>
                        Remove pan from the heat, add fresh herbs and grated cheese (and pepper flakes if using) and toss well. Garnish with more of the freshly grated Parmesan cheese and serve immediately.
                    </li>
                </ol>


            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <a href="img/recipes/recipe1.jpg" download="recipe1.jpg" target="_blank" class="btn btn-danger">Download</a>
            </div>
        </div>
    </div>
</div>
<!--end recipe1-->

<!--recipe2-->
<div class="modal fade" id="recipe2" tabindex="-1" role="dialog" aria-labelledby="recipe2Label" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="background-image: url('img/recipe2.png');background-repeat: no-repeat;">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="recipe2Label" style="color:#000000; text-align: center;">Simple Salad</h4>
            </div>
            <div class="modal-body" style="color:black;">
                <h4>Ingredients</h4>
                <ul class="list-group">
                    <li class="list-group-item">2 bags ready to use mesclun greens</li>
                    <li class="list-group-item">salt</li>
                    <li class="list-group-item">Freshly ground black pepper</li>
                    <li class="list-group-item">1 pint grape tomatoes</li>
                    <li class="list-group-item">1 cucumber halved and thinly sliced</li>
                    <li class="list-group-item">1 shallot, peeled and roughly chopped</li>
                    <li class="list-group-item">1 tablespoon chopped fresh basil leaves</li>
                    <li class="list-group-item">1/4 cup Marco's Russo balsamic vinegar or Sophia's Bianco Balsamic Vinegar</li>
                </ul>
                <h4>Instructions</h4>
                <ol>
                    <li>
                        Toss the greens, salt and pepper together with just enough vinaigrette to coat, about 1/2 cup.
                    </li>
                    <li>
                        Place them in a decorative bowl and top with the grape tomatoes and cucumber. Serve the remaining vinaigrette on the side.
                    </li>
                </ol>

                <h4>Balsamic Vinegar Instructions</h4>
                <ol>
                    <li>Put the shallot and basil into the bowl of a food processor fitted with a metal blade.</li>
                    <li>Pulse until the shallot is finely chopped.</li>
                    <li>Add the balsamic, sugar, salt and pepper and pulse again.</li>
                    <li>With the machine running, add the olive oil in a steady stream through the feed tube.</li>
                    <li>Pour the vinaigrette into a small bowl and serve.</li>
                </ol>


            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <a href="img/recipes/recipe2.jpg" download="recipe2.jpg" target="_blank" class="btn btn-danger">Download</a>
            </div>
        </div>
    </div>
</div>

<!--recipe3-->
<div class="modal fade" id="recipe3" tabindex="-1" role="dialog" aria-labelledby="recipe3Label" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="background-image: url('img/recipe3.png');background-repeat: no-repeat;">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="recipe3Label" style="color:#000000; text-align: center;">Popcorn</h4>
            </div>
            <div class="modal-body" style="color:black;">
                <h4>Ingredients</h4>
                <ul class="list-group">
                    <li class="list-group-item">1 cup popcorn kernels</li>
                    <li class="list-group-item">2 tablespoons Rosemary Infused Oil</li>
                    <li class="list-group-item">Salt</li>
                </ul>
                <h4>Instructions</h4>
                <ol>
                    <li>
                        Stir the popcorn kernels and 1/2 cup of the oil in a heavy large pot.</li>
                    <li>
                        Cover and cook over medium heat until the kernels have popped, shaking the pot halfway through cooking, about 3 minutes.
                    </li>
                    <li>
                        Immediately transfer the popcorn to a large serving bowl.
                    </li>
                    <li>
                        Toss the popcorn with the remaining 2 tablespoons of Rosemary Infused Olive Oil.
                    </li>
                    <li>
                        Sprinkle with salt, to taste, and serve
                    </li>
                </ol>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <a href="img/recipes/recipe3.jpg" download="recipe3.jpg" target="_blank" class="btn btn-danger">Download</a>
            </div>
        </div>
    </div>
</div>

<!--recipe 4-->
<div class="modal fade" id="recipe4" tabindex="-1" role="dialog" aria-labelledby="recipe4Label" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="background-image: url('img/recipe4.png');background-repeat: no-repeat;">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="recipe4Label" style="color:#000000; text-align: center;">Grilled Club Sandwich</h4>
            </div>
            <div class="modal-body" style="color:black;">
                <h4>Ingredients</h4>
                <ul class="list-group">
                    <li class="list-group-item">4 teaspoons Mayonnaise</li>
                    <li class="list-group-item">1 tablespoon Sage and Onion Extra Virgin Olive Oil</li>
                    <li class="list-group-item">4 teaspoons barbecue sauce</li>
                    <li class="list-group-item">4 slices French bread</li>
                    <li class="list-group-item">2/3 cup shredded cheddar cheese</li>
                    <li class="list-group-item">4 slices of deli ham</li>
                    <li class="list-group-item">4 slices of tomatoe</li>
                    <li class="list-group-item">4 thin slices of turkey</li>
                    <li class="list-group-item">2 tablespoons of soft butter</li>
                </ul>
                <h4>Instructions</h4>
                <ol>
                    <li>
                        Combine mayonnaise and barbecue sauce; spread over one side of each slice of bread.
                    </li>
                    <li>
                        Add 2 tablespoons of Sage and Onion Extra Virgin Olive Oil
                    <li>
                        Top with remaining bread.
                    </li>
                    <li>
                        Spread outside of sandwiches with butter.
                    </li>
                    <li>
                        In a skillet, toast sandwiches over medium heat until bread is lightly browned on both sides.
                    </li>
                </ol>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <a href="img/recipes/recipe4.jpg" download="recipe4.jpg" target="_blank" class="btn btn-danger">Download</a>
            </div>
        </div>
    </div>
</div>

<!--recipe5-->
<div class="modal fade" id="recipe5" tabindex="-1" role="dialog" aria-labelledby="recipe5Label" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="background-image: url('img/recipe5.png');background-repeat: no-repeat;">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="recipe5Label" style="color:#000000; text-align: center;">Garlic Potatoes</h4>
            </div>
            <div class="modal-body" style="color:black;">
                <h4>Ingredients</h4>
                <ul class="list-group">
                    <li class="list-group-item">3 pounds small red or white potatoes</li>
                    <li class="list-group-item">1/4 cup Garlic and Mushroom Olive Oil</li>
                    <li class="list-group-item">1 1/2 teaspoons salt</li>
                    <li class="list-group-item">1 teaspoon black pepper</li>
                    <li class="list-group-item">2 tablespoons minced garlic</li>
                    <li class="list-group-item">2 tablespoons minced fresh parsley</li>
                </ul>
                <h4>Instructions</h4>
                <ol>
                    <li>
                        Preheat the oven to 400 degrees F.
                    </li>
                    <li>
                        Cut the potatoes in half or quarters and place in a bowl with the olive oil, salt, pepper, and garlic; toss until the potatoes are well coated.</li>
                    </li>
                    <li>
                        Transfer the potatoes to a sheet pan and spread out into 1 layer.
                    </li>
                    <li>
                        Roast in the oven for 45 minutes to 1 hour or until browned and crisp.
                    </li>
                    <li>
                        Flip twice with a spatula during cooking in order to ensure even browning.
                    </li>
                    <li>
                        Remove the potatoes from the oven, toss with parsley, season to taste, and serve hot.
                    </li>

                </ol>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <a href="img/recipes/recipe5.jpg" download="recipe5.jpg" target="_blank" class="btn btn-danger">Download</a>
            </div>
        </div>
    </div>
</div>

<!--recipe6-->
<div class="modal fade" id="recipe6" tabindex="-1" role="dialog" aria-labelledby="recipe6Label" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="background-image: url('img/recipe6.png');background-repeat: no-repeat;">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="recipe6Label" style="color:#000000; text-align: center;">Roasted Butternut Squash</h4>
            </div>
            <div class="modal-body" style="color:black;">
                <h4>Ingredients</h4>
                <ul class="list-group">
                    <li class="list-group-item">1 butternut squash - peeled, seeded, and cut into 1-inch cubes</li>
                    <li class="list-group-item">2 tablespoons Pumkin Spice Balsamic</li>
                    <li class="list-group-item">2 cloves garlic, minced</li>
                    <li class="list-group-item">salt and ground black pepper to taste</li>
                </ul>
                <h4>Instructions</h4>
                <ol>
                    <li>
                        Preheat oven to 400 degrees F (200 degrees C).
                    </li>
                    <li>
                        Toss butternut squash with Pumpkin Spice balsamic and garlic in a large bowl. Season with salt and black pepper. Arrange coated squash on a baking sheet.
                    </li>
                    <li>
                        Roast in the preheated oven until squash is tender and lightly browned, 25 to 30 minutes.
                    </li>
                </ol>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <a href="img/recipes/recipe6.jpg" download="recipe6.jpg" target="_blank" class="btn btn-danger">Download</a>
            </div>
        </div>
    </div>
</div>

<!--recipe7-->
<div class="modal fade" id="recipe7" tabindex="-1" role="dialog" aria-labelledby="recipe7Label" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="background-image: url('img/recipe7.png');background-repeat: no-repeat;">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="recipe7Label" style="color:#000000; text-align: center;">Baked Acorn Squash</h4>
            </div>
            <div class="modal-body" style="color:black;">
                <h4>Ingredients</h4>
                <ul class="list-group">
                    <li class="list-group-item">2 acorn squash, halved and seeded</li>
                    <li class="list-group-item">2 teaspoons Pumpkin Spice Balsamic Vinegar</li>
                    <li class="list-group-item">salt and pepper to taste</li>
                    <li class="list-group-item">1/4 cup butter, diced</li>
                    <li class="list-group-item">6 tablespoons firmly packed brown sugar</li>
                    <li class="list-group-item">1/2 teaspoon ground cinnamon</li>
                </ul>
                <h4>Instructions</h4>
                <ol>
                    <li>
                        Preheat oven to 350 degrees F (175 degrees C).
                    </li>
                    <li>
                        Place squash in a shallow baking pan, cut side down.
                    </li>
                    <li>
                        Bake in preheated oven for 30 minutes, or until tender.
                    </li>
                    <li>
                        Turn cut side up; season with salt and pepper, dot with butter, add Pumpkin Spice Balsamic Vinegar and sprinkle with brown sugar and cinnamon.
                    </li>
                    <li>
                        Bake for 20 minutes more.
                    </li>
                </ol>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <a href="img/recipes/recipe7.jpg" download="recipe7.jpg" target="_blank" class="btn btn-danger">Download</a>
            </div>
        </div>
    </div>
</div>

<!--recipe8-->
<div class="modal fade" id="recipe8" tabindex="-1" role="dialog" aria-labelledby="recipe8Label" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="background-image: url('img/recipe8.png');background-repeat: no-repeat;">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="recipe8Label" style="color:#000000; text-align: center;">Sushi Roll</h4>
            </div>
            <div class="modal-body" style="color:black;">
                <h4>Ingredients</h4>
                <ul class="list-group">
                    <li class="list-group-item">2/3 cup uncooked short-grain white rice</li>
                    <li class="list-group-item">3 tablespoons Toasted Sesame Oil</li>
                    <li class="list-group-item">3 tablespoons white sugar</li>
                    <li class="list-group-item">1 1/2 teaspoons salt</li>
                    <li class="list-group-item">4 sheets nori seaweed sheets</li>
                    <li class="list-group-item">1/2 cucumber, peeled, cut into small strips</li>
                    <li class="list-group-item">2 tablespoons pickled ginger</li>
                    <li class="list-group-item">1 avocado</li>
                    <li class="list-group-item">1/2 pound imitation crabmeat, flaked</li>
                </ul>
                <h4>Instructions</h4>
                <ol>
                    <li>
                        In a medium saucepan, bring 1 1/3 cups water to a boil. Add rice, and stir. Reduce heat, cover, and simmer for 20 minutes. In a small bowl, mix the Toasted Sesame Oil, sugar ,and salt. Blend the mixture into the rice.
                    </li>
                    <li>
                        Preheat oven to 300 degrees F (150 degrees C). On a medium baking sheet, heat nori in the preheated oven 1 to 2 minutes, until warm.
                    </li>
                    <li>
                        Center one sheet nori on a bamboo sushi mat. Wet your hands. Using your hands, spread a thin layer of rice on the sheet of nori, and press into a thin layer. Arrange 1/4 of the cucumber, ginger, avocado, and imitation crabmeat in a line down the center of the rice. Lift the end of the mat, and gently roll it over the ingredients, pressing gently. Roll it forward to make a complete roll. Repeat with remaining ingredients.
                    </li>
                    <li>
                        Cut each roll into 4 to 6 slices using a wet, sharp knife.
                    </li>
                </ol>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <a href="img/recipes/recipe8.jpg" download="recipe8.jpg" target="_blank" class="btn btn-danger">Download</a>
            </div>
        </div>
    </div>
</div>

<!--recipe9-->
<div class="modal fade" id="recipe9" tabindex="-1" role="dialog" aria-labelledby="recipe9Label" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="background-image: url('img/recipe9.png');background-repeat: no-repeat;">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="recipe9Label" style="color:#000000; text-align: center;">Extra Virgin Olive Oil Herb Dip</h4>
            </div>
            <div class="modal-body" style="color:black;">
                <h4>Ingredients</h4>
                <ul class="list-group">
                    <li class="list-group-item">1/4 teaspoon oregano</li>
                    <li class="list-group-item">1/4 teaspoon basil</li>
                    <li class="list-group-item">1/4 teaspoon rosemary</li>
                    <li class="list-group-item">1/4 teaspoon salt (or according to your taste)</li>
                    <li class="list-group-item">black pepper</li>
                    <li class="list-group-item">1 pinch red pepper flakes</li>
                    <li class="list-group-item">2 cloves fresh garlic, minced</li>
                    <li class="list-group-item">1/4 cup Tuscan Herb Olive Oil, Herbs d' Provence or Garlic Infused Olive Oil</li>
                </ul>
                <h4>Instructions</h4>
                <ol>
                    <li>
                        In a small bowl, mix together the dry spices. Add crushed garlic, stirring to combine and moisten the herbs.
                    </li>
                    <li>
                        Transfer herb mixture to a small dish or bowl. Pour olive oil over herb mixture.
                    </li>
                    <li>
                        Serve with fresh bread.
                    </li>
                </ol>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <a href="img/recipes/recipe9.jpg" download="recipe9.jpg" target="_blank" class="btn btn-danger">Download</a>
            </div>
        </div>
    </div>
</div>

<!--recipe10-->
<div class="modal fade" id="recipe10" tabindex="-1" role="dialog" aria-labelledby="recipe10Label" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="background-image: url('img/recipe10.png');background-repeat: no-repeat;">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="recipe10Label" style="color:#000000; text-align: center;">Peruvian Anticuchos</h4>
            </div>
            <div class="modal-body" style="color:black;">
                <h4>Ingredients</h4>
                <ul class="list-group">
                    <li class="list-group-item">2 1/2 lbs of fresh beef heart, thinly sliced and cut into squares of about 2 inches long</li>
                    <li class="list-group-item">1 cup Jalapeno Lime Dark Balsamic Vinegar</li>
                    <li class="list-group-item">4 tablespoons ground cumin</li>
                    <li class="list-group-item">1 teaspoon ground pepper</li>
                    <li class="list-group-item">1 teaspoon salt</li>
                    <li class="list-group-item">5 big fresh garlic cloves</li>
                    <li class="list-group-item">2 tablespoons of finely minced fresh parsley</li>
                    <li class="list-group-item">tablespoons of finely minced fresh cilantro</li>
                    <li class="list-group-item">4 dried chilies</li>
                    <li class="list-group-item">1 1/2 cups Snappy Jalapeno Olive Oil</li>
                    <li class="list-group-item">10 -12 ears corn on the cob, cooked (save some of the husk)</li>
                    <li class="list-group-item">10 -12 boiled potatoes, peeled</li>
                </ul>
                <h4>Instructions</h4>
                <ol>
                    <li>
                        Place the pieces of heart in a glass or ceramic tray.
                    </li>
                    <li>
                        Previously, soak the chiles in hot water until they are soft, devein and take the seeds off. If you want you can keep some of the seeds to make it spicier. In Peru we use aji panca, but this works well too.
                    </li>
                    <li>
                        Blend the vinegar, garlic, aji panca (or dried chile), and all other ingredients with 1/2 cup of the oil until you have a soft paste
                    </li>
                    <li>
                        Pour it on the pieces of heart and distribute evenly so all pieces are well covered and can absorb the marinade. Cover and let sit in the refrigerator for about 30 minutes. Do not let them marinade for too long or they will dry out because of the vinegar.
                    </li>
                    <li>
                        In the meantime, place thick bamboo skewers to soak in water so they don't burn when they go on the grill.
                    </li>
                    <li>
                        Use a charcoal grill and make sure the coals are very hot before you start.
                    </li>
                    <li>
                        Stick three or four pieces of heart in each skewer, so that the meat lays flat.
                    </li>
                    <li>
                        Save the rest of the marinade in a cup or small bowl and add the rest of the oil to it, mixing well. This will be used for basting the anticuchos on the grill.
                    </li>
                    <li>
                        Tie some pieces of fresh corn husk with a string made out of some more husk and shred them half the way to make a kind of brush and use it for basting. (You can use dry husk too but you need to soak it for a while in warm water to make it flexible and then pat it dry before you use it).
                    </li>
                    <li>
                        When the coals are ready and the grill is hot place the anticuchos flat on the grill and baste them generously with the leftover marinade and oil mix.
                    </li>
                    <li>
                        This will drip and cause the coals to flame, make sure it flames over because this is what gives the anticuchos their distinctive flavor
                    </li>
                    <li>
                        Let anticuchos cook for about 1 minute and a half on each side. DO NOT OVERCOOK! They will dry out and become tough. Medium or medium well is fine. Turn them over continuously and keep basting and flaming until they are done.
                    </li>
                    <li>
                        Hold two or three at a time to turn them over quickly
                    </li>
                    <li>
                        At the same time, place the pieces of corn and potatoes on a corner of the grill, baste them with the same marinade and allow them to be flamed too.
                    </li>
                    <li>
                        Serve three skewers in each plate, accompanied by one piece of corn and one potato. You can also cut the potatoes in half (across not length wise) and stick a piece at the end of each skewer.
                    </li>
                    <li>
                        Serve hot, right out of the grill.
                    </li>
                    <li>
                        What takes the longest time is to slice and cut the heart in squares and stick the pieces on the skewers; as you get practice doing this it will be faster, allow yourself some more time of preparation the first couple of times.
                    </li>
                </ol>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <a href="img/recipes/recipe10.jpg" download="recipe10.jpg" target="_blank" class="btn btn-danger">Download</a>
            </div>
        </div>
    </div>
</div>

<!--recipe11-->
<div class="modal fade" id="recipe11" tabindex="-1" role="dialog" aria-labelledby="recipe11Label" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="background-image: url('img/recipe11.png');background-repeat: no-repeat;">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="recipe11Label" style="color:#000000; text-align: center;">Mexican Chorizo</h4>
            </div>
            <div class="modal-body" style="color:black;">
                <h4>Ingredients</h4>
                <ul class="list-group">
                    <li class="list-group-item">2 pounds boneless pork butt (shoulder), cut into 3/4-inch pieces</li>
                    <li class="list-group-item">1 1/2 tablespoons crushed Aleppo peppers</li>
                    <li class="list-group-item">1 1/2 tablespoons chili powder</li>
                    <li class="list-group-item">4 cloves garlic, minced</li>
                    <li class="list-group-item">2 teaspoons salt</li>
                    <li class="list-group-item">1 teaspoon black pepper</li>
                    <li class="list-group-item">1/2 teaspoon dried oregano</li>
                    <li class="list-group-item">1/2 teaspoon ground cumin</li>
                    <li class="list-group-item">1/4 teaspoon ground cloves</li>
                    <li class="list-group-item">1/4 teaspoon ground coriander</li>
                    <li class="list-group-item">1/2 cup distilled white vinegar</li>
                    <li class="list-group-item">2 tablespoons water</li>
                    <li class="list-group-item">1 teaspoon Smokin' Chipotle Olive Oil</li>
                </ul>
                <h4>Instructions</h4>
                <ol>
                    <li>
                        Place the pork, Aleppo pepper, chili powder, garlic, salt, black pepper, oregano, cumin, cloves, and coriander into a bowl, and lightly toss the pork with the seasonings until thoroughly blended. Cover the bowl, and refrigerate the meat, your meat grinder's head assembly, and grinder hopper for 1 hour.
                    </li>
                    <li>
                        Fill a large mixing bowl with ice cubes, and place a smaller metal bowl in the ice cubes to catch the ground meat. Assemble the chilled meat grinder, and grind the pork and seasonings using a coarse cutting plate. Return ground meat to refrigerator for 30 minutes. Lightly stir the ground pork with the vinegar and water until thoroughly mixed, form into patties, and refrigerate overnight, covered, to let flavors develop.
                    </li>
                    <li>
                        Heat vegetable oil in a heavy skillet over medium-low heat, and pan-fry the patties until browned and no longer pink in the middle, 5 to 8 minutes per side.
                    </li>
                </ol>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <a href="img/recipes/recipe11.jpg" download="recipe11.jpg" target="_blank" class="btn btn-danger">Download</a>
            </div>
        </div>
    </div>
</div>

<!--recipe12-->
<div class="modal fade" id="recipe12" tabindex="-1" role="dialog" aria-labelledby="recipe12Label" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="background-image: url('img/recipe12.png');background-repeat: no-repeat;">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="recipe12Label" style="color:#000000; text-align: center;">Pollo Habanero</h4>
            </div>
            <div class="modal-body" style="color:black;">
                <h4>Ingredients</h4>
                <ul class="list-group">
                    <li class="list-group-item">4-6 Chicken Breast</li>
                    <li class="list-group-item">1 cups apple cider vinegar</li>
                    <li class="list-group-item">3 Each Mangos rough chopped </li>
                    <li class="list-group-item">2 tablespoon Molasses</li>
                    <li class="list-group-item">2 tablespoon Worcestershire sauce</li>
                    <li class="list-group-item">2 tablespoon mustard Dijon or dry</li>
                    <li class="list-group-item">3 - 5 Habanero peppers; de-seeded and diced </li>
                    <li class="list-group-item">1/4 cups brown sugar </li>
                    <li class="list-group-item">2 tablespoon Fiery Habanero Olive Oil</li>
                    <li class="list-group-item">1 Juice from lime</li>
                    <li class="list-group-item">1 small Onion chopped</li>
                    <li class="list-group-item">salt</li>
                    <li class="list-group-item">3 cloves garlic chopped</li>
                    <li class="list-group-item">1 tablespoon cumin</li>
                </ul>
                <h4>Instructions</h4>
                <ol>
                    <li>
                        To make the sauce, start by heating up your oil in a large pan. Add the onion, garlic, and peppers and sautee for 3-5 minutes on medium heat until they are soft. Then add the mangos and stir for 5 more minutes. The mangos should release a lot of liquid and make a kind of soup.
                    </li>
                    <li>
                        After 5 minutes, you can add all your other ingredients.
                    </li>
                    <li>
                        Then let the sauce simmer on low for 15-20 minutes. It should reduce by about 1/4, but keep a close eye on it so it doesnt burn or scorch!
                        Stir it frequently.
                    </li>
                    <li>
                        Sear the chicken on high heat in a few Teaspoons of Fiery Hababnero Olive Oil in saute pan until brown on both sides. Transfer chicken to a large baking dish, cover with sauce and stick the whole thing in the oven for about an hour (30 minutes covered and 30 minutes uncovered) at 325 degrees. Serve over rice.
                    </li>
                </ol>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <a href="img/recipes/recipe12.jpg" download="recipe12.jpg" target="_blank" class="btn btn-danger">Download</a>
            </div>
        </div>
    </div>
</div>

<!--recipe13-->
<div class="modal fade" id="recipe13" tabindex="-1" role="dialog" aria-labelledby="recipe13Label" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="background-image: url('img/recipe13.png');background-repeat: no-repeat;">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="recipe13Label" style="color:#000000; text-align: center;">Tomato, red onion and balsamic salsa</h4>
            </div>
            <div class="modal-body" style="color:black;">
                <h4>Ingredients</h4>
                <ul class="list-group">
                    <li class="list-group-item">2 1/2 tablespoons extra virgin olive oil</li>
                    <li class="list-group-item">1 garlic clove, crushed</li>
                    <li class="list-group-item">1 tablespoon green chili white balsamic vinegar</li>
                    <li class="list-group-item">500g grape or mini roma tomatoes, roughly chopped</li>
                    <li class="list-group-item">1 small red onion, finely diced</li>
                    <li class="list-group-item">1/4 cup flat-leaf parsley leaves, chopped</li>
                </ul>
                <h4>Instructions</h4>
                <ol>
                    <li>
                        Place extra virgin olive oil, garlic and green chili white balsamic vinegar in a bowl. Add tomatoes, onion and parsley. Season with salt and pepper and stir to combine. Stand for 10 minutes. Serve with barbecued steak or lamb kebabs.
                    </li>
                </ol>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <a href="img/recipes/recipe13.jpg" download="recipe13.jpg" target="_blank" class="btn btn-danger">Download</a>
            </div>
        </div>
    </div>
</div>


<!--recipe14-->
<div class="modal fade" id="recipe14" tabindex="-1" role="dialog" aria-labelledby="recipe14Label" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="background-image: url('img/recipe14.png');background-repeat: no-repeat;">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="recipe14Label" style="color:#000000; text-align: center;">Balsamic bacon onion burritos</h4>
            </div>
            <div class="modal-body" style="color:black;">
                <h4>Ingredients</h4>
                <ul class="list-group">
                    <li class="list-group-item">4 tablespoons butter</li>
                    <li class="list-group-item">2 onions, sliced</li>
                    <li class="list-group-item">Salt</li>
                    <li class="list-group-item">8 leaves basil, sliced</li>
                    <li class="list-group-item">1 cup chicken broth</li>
                    <li class="list-group-item">2 tablespoons cumin</li>
                    <li class="list-group-item">8 tablespoons Chocolate Jalapeno balsamic vinegar</li>
                    <li class="list-group-item">8 strips bacon</li>
                    <li class="list-group-item">4 tortillas</li>
                    <li class="list-group-item">Guacamole</li>
                    <li class="list-group-item">1 red onion, minced</li>
                </ul>
                <h4>Instructions</h4>
                <ol>
                    <li>
                        Melt the butter in a skillet over medium-high heat.
                    </li>
                    <li>
                        Once melted, add the onions and a healthy pinch of salt. Cook 8 minutes.
                    </li>
                    <li>
                        Add the basil, then set the heat to low.
                    </li>
                    <li>
                        Cover the onions and let them cook for 1 hour.
                    </li>
                    <li>
                        After about 30 minutes, pour the navy beans, broth, cumin and a healthy pinch of salt into a saucepan.
                    </li>
                    <li>
                        Bring the broth to a boil, then reduce to a simmer.
                    </li>
                    <li>
                        Cook until the onions have cooked 1 hour.
                    </li>
                    <li>
                        Stir in half the Chocolate Jalapeno balsamic vinegar into the onions and let them keep cooking covered.
                    </li>
                    <li>
                        In a skillet over medium heat, add the bacon and let it cook 5 minutes.
                    </li>
                    <li>
                        Add the rest of the balsamic and cook for another 4 minutes.
                    </li>
                    <li>
                        After the bacon is done, chop it up and pour it and its gravy into the navy beans.
                    </li>
                    <li>
                        Finally, construct the burritos by spreading the beans over a tortilla and then topping with onions.
                    </li>
                    <li>
                        Repeat with the other shells.
                    </li>
                </ol>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <a href="img/recipes/recipe14.jpg" download="recipe14.jpg" target="_blank" class="btn btn-danger">Download</a>
            </div>
        </div>
    </div>
</div>

<!--recipe15-->
<div class="modal fade" id="recipe15" tabindex="-1" role="dialog" aria-labelledby="recipe15Label" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="background-image: url('img/recipe15.png');background-repeat: no-repeat;">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="recipe15Label" style="color:#000000; text-align: center;">Marinated Olive & Cheese Ring Recipe</h4>
            </div>
            <div class="modal-body" style="color:black;">
                <h4>Ingredients</h4>
                <ul class="list-group">
                    <li class="list-group-item">1 package (8 ounces) cold cream cheese</li>
                    <li class="list-group-item">1 package (10 ounces) sharp white cheddar cheese, cut into 1/4-inch slices</li>
                    <li class="list-group-item">1/3 cup pimiento-stuffed olives</li>
                    <li class="list-group-item">1/3 cup pitted Greek olives</li>
                    <li class="list-group-item">1/4 cup All Star Garlic Cilantro balsamic vinegar</li>
                    <li class="list-group-item">1/4 cup Luscious Lemon olive oil</li>
                    <li class="list-group-item">1 tablespoon minced fresh parsley</li>
                    <li class="list-group-item">1 tablespoon minced fresh basil or 1 teaspoon dried basil</li>
                    <li class="list-group-item">2 garlic cloves, minced</li>
                    <li class="list-group-item">1 jar (2 ounces) pimiento strips, drained and chopped</li>
                    <li class="list-group-item">Toasted French bread baguette slices</li>
                </ul>
                <h4>Instructions</h4>
                <ol>
                    <li>
                        Cut cream cheese lengthwise in half; cut each half into 1/4-in. slices. On a serving plate, arrange cheeses upright in a ring, alternating cheddar and cream cheese slices. Place olives in center.
                    </li>
                    <li>
                        In a small bowl, whisk vinegar, oil, parsley, basil and garlic until blended; drizzle over cheeses and olives. Sprinkle with pimientos. Refrigerate, covered, at least 8 hours or overnight. Serve with baguette slices. Yield: 16 servings.
                    </li>

                </ol>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <a href="img/recipes/recipe15.jpg" download="recipe15.jpg" target="_blank" class="btn btn-danger">Download</a>
            </div>
        </div>
    </div>
</div>

<!--recipe16-->
<div class="modal fade" id="recipe16" tabindex="-1" role="dialog" aria-labelledby="recipe16Label" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="background-image: url('img/recipe16.png');background-repeat: no-repeat;">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="recipe16Label" style="color:#000000; text-align: center;">Balsamic Pork Chops</h4>
            </div>
            <div class="modal-body" style="color:black;">
                <h4>Ingredients</h4>
                <ul class="list-group">
                    <li class="list-group-item">1 (6.2-ounce) package fast-cooking long-grain and wild rice mix</li>
                    <li class="list-group-item">3 tablespoons all-purpose flour</li>
                    <li class="list-group-item">1 teaspoon chopped fresh rosemary</li>
                    <li class="list-group-item">1/2 teaspoon salt</li>
                    <li class="list-group-item">1/2 teaspoon pepper</li>
                    <li class="list-group-item">6 (3/4-inch-thick) boneless pork chops</li>
                    <li class="list-group-item">2 tablespoons Rapturous Lime olive oil</li>
                    <li class="list-group-item">2 garlic cloves, pressed</li>
                    <li class="list-group-item">1 (14 1/2-ounce) can chicken broth</li>
                    <li class="list-group-item">1/3 cup Dark Chocolate Divine balsamic vinegar</li>
                    <li class="list-group-item">Garnish: fresh rosemary sprigs</li>
                </ul>
                <h4>Instructions</h4>
                <ol>
                    <li>
                        Cook rice according to package directions; keep warm.
                    </li>
                    <li>
                        Combine flour, 1 teaspoon rosemary, salt, and pepper. Dredge pork chops in flour mixture.
                    </li>
                    <li>
                        Melt butter with oil in a large skillet over medium-high heat; add garlic, and sauté 1 minute.
                    </li>
                    <li>
                        Add pork chops, and cook 4 minutes on each side or until golden. Remove pork chops.
                    </li>
                    <li>
                        Add broth and vinegar, stirring to loosen particles from bottom of skillet.
                    </li>
                    <li>
                        Cook 6 minutes or until liquid is reduced by half. Add pork chops, and cook 5 minutes or until done.
                    </li>
                    <li>
                        Serve over rice. Garnish, if desired.
                    </li>

                </ol>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <a href="img/recipes/recipe16.jpg" download="recipe16.jpg" target="_blank" class="btn btn-danger">Download</a>
            </div>
        </div>
    </div>
</div>

<!--recipe17-->
<div class="modal fade" id="recipe17" tabindex="-1" role="dialog" aria-labelledby="recipe17Label" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="background-image: url('img/recipe17.png');background-repeat: no-repeat;">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="recipe17Label" style="color:#000000; text-align: center;">Strawberry Sundae with Balsamic Vinegar and Pistachios</h4>
            </div>
            <div class="modal-body" style="color:black;">
                <h4>Ingredients</h4>
                <ul class="list-group">
                    <li class="list-group-item">12 strawberries, hulled and sliced</li>
                    <li class="list-group-item">1 pint of vanilla ice cream.</li>
                    <li class="list-group-item">½ cup of Blissful Chocolate with Orange balsamic vinegar</li>
                    <li class="list-group-item">2 tbsp sugar.</li>
                    <li class="list-group-item">1 vanilla bean, scraped</li>
                    <li class="list-group-item">A handful of shelled and salted pistachios</li>
                </ul>
                <h4>Instructions</h4>
                <ol>
                    <li>
                        In a non-reactive saucepan, bring vinegar, sugar, and vanilla bean guts to a boil. Use low heat.
                    </li>
                    <li>
                        Top scoops of ice cream generously with the strawberries and a sprinkling of pistachios.  Finish everything with a generous drizzle of balsamic syrup.
                    </li>
                </ol>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <a href="img/recipes/recipe17.jpg" download="recipe17.jpg" target="_blank" class="btn btn-danger">Download</a>
            </div>
        </div>
    </div>
</div>

<!--recipe18-->
<div class="modal fade" id="recipe18" tabindex="-1" role="dialog" aria-labelledby="recipe18Label" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="background-image: url('img/recipe18.png');background-repeat: no-repeat;">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="recipe18Label" style="color:#000000; text-align: center;">Corn on the Cob with Parmesan Cheese</h4>
            </div>
            <div class="modal-body" style="color:black;">
                <h4>Ingredients</h4>
                <ul class="list-group">
                    <li class="list-group-item">1/2 cup Rapturous Lime olive oil</li>
                    <li class="list-group-item">2 garlic cloves, chopped</li>
                    <li class="list-group-item">1/2 cup freshly grated Parmesan</li>
                    <li class="list-group-item">2 tablespoons chopped fresh Italian parsley leaves</li>
                    <li class="list-group-item">1/2 teaspoon salt</li>
                    <li class="list-group-item">6 ears yellow corn, husked, halved crosswise</li>
                </ul>
                <h4>Instructions</h4>
                <ol>
                    <li>
                        Heat the oil in a heavy small skillet over medium heat. Add the garlic and saute until tender and fragrant, about 1 minute.
                    </li>
                    <li>
                        Remove the skillet from the heat and cool. Set aside 2 tablespoons of the Parmesan cheese and stir the rest into the garlic mixture, along with the parsley and salt.
                    </li>
                    <li>
                        Cook the corn in a large saucepan of boiling salted water until crisp-tender, about 5 minutes.
                    </li>
                    <li>
                        Using tongs, transfer the corn to a platter.
                    </li>
                    <li>
                        Brush the cheese mixture over the hot corn and serve. Sprinkle the remaining 2 tablespoons of cheese on top of the corn.
                    </li>
                </ol>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <a href="img/recipes/recipe18.jpg" download="recipe18.jpg" target="_blank" class="btn btn-danger">Download</a>
            </div>
        </div>
    </div>
</div>

<!--recipe19-->
<div class="modal fade" id="recipe19" tabindex="-1" role="dialog" aria-labelledby="recipe19Label" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="background-image: url('img/recipe19.png');background-repeat: no-repeat;">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="recipe19Label" style="color:#000000; text-align: center;">Linguine with Shrimp and Lemon Oil</h4>
            </div>
            <div class="modal-body" style="color:black;">
                <h4>Ingredients</h4>
                <ul class="list-group">
                    <li class="list-group-item">1/2 cup Lemon Pepper Infused olive oil</li>
                    <li class="list-group-item">1 lemon, zested</li>
                    <li class="list-group-item">1 pound linguine pasta</li>
                    <li class="list-group-item">2 tablespoons olive oil</li>
                    <li class="list-group-item">2 shallots, diced</li>
                    <li class="list-group-item">2 garlic cloves, minced</li>
                    <li class="list-group-item">16 ounces frozen shrimp</li>
                    <li class="list-group-item">1/4 cup lemon juice (about 2 lemons)</li>
                    <li class="list-group-item">1 lemon, zested</li>
                    <li class="list-group-item">1 teaspoon salt</li>
                    <li class="list-group-item">1/2 teaspoon freshly ground black pepper</li>
                    <li class="list-group-item">3 ounces arugula (about 3 packed cups)</li>
                    <li class="list-group-item">1/4 cup chopped fresh flat-leaf parsley</li>
                </ul>
                <h4>Instructions</h4>
                <ol>
                    <li>
                        Combine the olive oil and the lemon zest in a small bowl and reserve.
                    </li>
                    <li>
                        Bring a large pot of salted water to a boil over high heat. Add the pasta and cook until tender but still firm to the bite, stirring occasionally, about 8 to 10 minutes. Drain pasta, reserving 1 cup of the cooking liquid.
                    </li>
                    <li>
                        Meanwhile, in a large, heavy skillet warm the olive oil over medium heat. Add the shallots and garlic and cook for 2 minutes. Add the shrimp and cook until pink, about 5 minutes. Add the cooked linguine, lemon juice, lemon zest, salt, and pepper. Toss to combine. Turn off the heat and add the arugula. Using a mesh sieve, strain the lemon zest out of the reserved lemon olive oil and add the oil to the pasta. The zest can be discarded. Add some of the cooking water to desired consistency. Add the chopped parsley to the pasta and toss to combine. Serve immediately.
                    </li>
                </ol>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <a href="img/recipes/recipe19.jpg" download="recipe19.jpg" target="_blank" class="btn btn-danger">Download</a>
            </div>
        </div>
    </div>
</div>

<!--recipe20-->
<div class="modal fade" id="recipe20" tabindex="-1" role="dialog" aria-labelledby="recipe20Label" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="background-image: url('img/recipe20.png');background-repeat: no-repeat;">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="recipe20Label" style="color:#000000; text-align: center;">Olive Oil Ice Cream</h4>
            </div>
            <div class="modal-body" style="color:black;">
                <h4>Ingredients</h4>
                <ul class="list-group">
                    <li class="list-group-item">1 3/4 cups whole milk</li>
                    <li class="list-group-item">¼ cup heavy cream</li>
                    <li class="list-group-item">¼ teaspoon kosher salt</li>
                    <li class="list-group-item">½ cup plus 2 Tbsp. sugar</li>
                    <li class="list-group-item">4 large egg yolks</li>
                    <li class="list-group-item">¼ cup extra-virgin olive oil</li>

                </ul>
                <h4>Instructions</h4>
                <ol>
                    <li>
                        Bring milk, cream, salt, and ½ cup sugar just to a simmer in a medium saucepan, stirring to dissolve sugar; remove from heat.
                    </li>
                    <li>
                        Whisk egg yolks and 2 Tbsp. sugar in a medium bowl until pale, about 2 minutes. Gradually whisk ½ cup hot milk mixture into yolks. Whisk yolk mixture into remaining milk mixture in saucepan. Cook over medium heat, stirring constantly, until thick enough to coat the back of a wooden spoon, 2–3 minutes. Strain custard through a fine-mesh sieve into a medium bowl set in a large bowl of ice water; whisk in oil. Let cool, stirring occasionally. Process custard in an ice cream maker according to manufacturer’s instructions. Transfer ice cream to an airtight container, cover, and freeze until firm, at least 4 hours.
                    </li>
                </ol>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <a href="img/recipes/recipe20.jpg" download="recipe20.jpg" target="_blank" class="btn btn-danger">Download</a>
            </div>
        </div>
    </div>
</div>

<!--recipe21-->
<div class="modal fade" id="recipe21" tabindex="-1" role="dialog" aria-labelledby="recipe21Label" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="background-image: url('img/recipe21.png');background-repeat: no-repeat;">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="recipe21Label" style="color:#000000; text-align: center;">Olive Oil Martini</h4>
            </div>
            <div class="modal-body" style="color:black;">
                <h4>Ingredients</h4>
                <ul class="list-group">
                    <li class="list-group-item">1 orange segment</li>
                    <li class="list-group-item">3 to5 fresh basil leaves</li>
                    <li class="list-group-item">3 oz vodka (English uses Skyy, but any premium vodka will do)</li>
                    <li class="list-group-item">½ oz dry vermouth</li>
                    <li class="list-group-item">½ oz Cointreau</li>
                    <li class="list-group-item">¼ oz simple syrup</li>
                    <li class="list-group-item">¼ oz Tantalizing Orange Olive Oil</li>
                </ul>
                <h4>Instructions</h4>
                <ol>
                    <li>
                        Muddle the orange segment and basil leaves in a mixing tin. Add the vodka, Cointreau, vermouth, simple syrup, olive oil, and ice, and shake well. Strain into a martini glass and float an additional drop of olive oil over the top.
                    </li>
                </ol>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <a href="img/recipes/recipe21.jpg" download="recipe21.jpg" target="_blank" class="btn btn-danger">Download</a>
            </div>
        </div>
    </div>
</div>

<!--recipe22-->
<div class="modal fade" id="recipe22" tabindex="-1" role="dialog" aria-labelledby="recipe22Label" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="background-image: url('img/recipe22.png');background-repeat: no-repeat;">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="recipe22Label" style="color:#000000; text-align: center;">Muddled Berry-Jalapeno Margarita</h4>
            </div>
            <div class="modal-body" style="color:black;">
                <h4>Ingredients</h4>
                <ul class="list-group">
                    <li class="list-group-item">Sea Salt</li>
                    <li class="list-group-item">2 lime wedges</li>
                    <li class="list-group-item">¼-inch slices of lime</li>
                    <li class="list-group-item">¼-inch slices of orange</li>
                    <li class="list-group-item">2 raspberries</li>
                    <li class="list-group-item">2 blackberries</li>
                    <li class="list-group-item">4 ounces tequila</li>
                    <li class="list-group-item">2 ounces Cointreau</li>
                    <li class="list-group-item">1 teaspoon Jalapeno Lime Balsamic Vinegar</li>
                    <li class="list-group-item">2 ounces fresh lime juice</li>
                    <li class="list-group-item">2 ounces fresh orange juice</li>
                </ul>
                <h4>Instructions</h4>
                <ol>
                    <li>
                        Pour sea salt into a shallow bowl.
                    </li>
                    <li>
                        using 8- to 10-ounce rock glasses, rub the rims of the glasses with a wedge of lime and dip into the sea salt.
                    </li>
                    <li>
                        Add 2 quarter-slices of orange, 1 quarter-slice of lime, a few berries and muddle slightly. Add ice and set aside.
                    </li>
                    <li>
                        In a small pitcher, add tequila, Cointreau, juices and balsamic vinegar. Stir until blended.
                    </li>
                    <li>
                        Pour over ice, and garnish with a few berries, a wedge of lime and a stirrer for each glass.
                    </li>
                    <li>
                        Makes 2 servings
                    </li>
                </ol>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <a href="img/recipes/recipe22.jpg" download="recipe22.jpg" target="_blank" class="btn btn-danger">Download</a>
            </div>
        </div>
    </div>
</div>


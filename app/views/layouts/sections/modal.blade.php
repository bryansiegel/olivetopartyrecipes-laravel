<!-- Modal -->
@foreach($recipes as $recipe)
<div class="modal fade" id="{{ $recipe->id }}" tabindex="-1" role="dialog" aria-labelledby="1Label" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="background-image: url('img/{{ $recipe->img }}');background-repeat: no-repeat;">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="recipe1Label" style="color:#000000; text-align: center;">{{ $recipe->title }}</h4>
            </div>
            <div class="modal-body" style="color:black;">
                <h4>Ingredients</h4>
                <ul class="list-group">
                    @foreach($recipe->ingredients as $ingredient)
                    <li class="list-group-item">{{ $ingredient->ingredient }}</li>
                    @endforeach
                </ul>
                <h4>Instructions</h4>
                <ol>
                    @foreach($recipe->instructions as $instruction)
                    <li>{{ $instruction->instruction }}</li>
                    @endforeach
                </ol>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <a href="img/recipes/recipe1.jpg" download="recipe1.jpg" target="_blank" class="btn btn-danger">Download</a>
            </div>
        </div>
    </div>
</div>
@endforeach
<!--end modal-->

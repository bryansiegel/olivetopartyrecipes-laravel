<section id="recipes" class="content-section text-center">
    <div class="recipes-section">
        <div class="container" style="background: rgba(20, 54, 54, 0.5);">
            <div>
                <h2>Recipes</h2>

                <p>Don't know what to do with your O'Live To Party Gift Set? We've got you covered. Here's a list of recipes recommended for your O'Live To Party Gift Box.</p>

                <table class="table">

                    <tr>
                        <thead>
                        <td>Recipe</td>
                        <td>Pairs With</td>
                        <td>Description</td>
                        <td></td>
                        </thead>
                    </tr>

                    @foreach($recipes as $recipe)
                        <tr>
                            <td>{{ $recipe->title }}</td>
                            <td>{{ $recipe->pairs }}</td>
                            <td>{{ $recipe->description }}</td>
                            <td>
                                <button class="btn btn-default btn-sm" data-toggle="modal" data-target="#{{ $recipe->id }}">
                                    View
                                </button>
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </div>
</section>
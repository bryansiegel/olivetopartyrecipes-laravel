<section id="about" class="container content-section text-center">
    <div class="row">
        <div class="col-lg-8 col-lg-offset-2">
            <h2>About O'Live To Party</h2>

            <p>So you want to party down with some oils and vinegars eh but you can't cook. We can take care of that for you. We can help you cook like your a master chef in no time. </p>

            <p>Check out some of our premier recipes to get you going.</p>
        </div>
    </div>
</section>
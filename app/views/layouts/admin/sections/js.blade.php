{{ HTML::script('js/admin/jquery.min.js') }}
{{ HTML::script('js/admin/bootstrap.min.js') }}
{{ HTML::script('js/admin/metisMenu.min.js') }}
{{ HTML::script('js/admin/raphael-min.js') }}
{{ HTML::script('js/admin/morris.min.js') }}
{{ HTML::script('js/admin/morris-data.js') }}

<!-- Custom Theme JavaScript -->
{{ HTML::script('js/admin/sb-admin-2.js') }}
<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="/admin">O'Live To Party</a>
    </div>
    <!-- /.navbar-header -->
    <ul class="nav navbar-top-links navbar-right">
<li><a href=""><i class="fa fa-user fa-fw"></i>Profile</a></li>
<li><a href="admin/logout"><i class="fa fa-sign-out fa-fw"></i>Logout</a></li>

    </ul>
    <!-- /.navbar-top-links -->

    <div class="navbar-default sidebar" role="navigation">
        <div class="sidebar-nav navbar-collapse">
            <ul class="nav" id="side-menu">
                <li>
                    <a href="/admin"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                </li>
                <li><a href="/users"><i class="fa fa-user fa-fw"></i> Users</a></li>

                {{--<li>--}}
                    {{--<a href=""><i class="fa fa-gear fa-fw"></i> Tables</a>--}}
                {{--</li>--}}
                <li>
                    <a href="/recipes"><i class="fa fa-edit fa-fw"></i> Recipes</a>
                </li>
            </ul>
        </div>
        <!-- /.sidebar-collapse -->
    </div>
    <!-- /.navbar-static-side -->
</nav>
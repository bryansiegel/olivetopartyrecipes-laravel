<!DOCTYPE html>
<html lang="en">
<head>
@include('layouts.admin.sections.head')
</head>
<body>
<div id="wrapper">
    <!-- Navigation -->
@include('layouts.admin.sections.nav')
    {{--main section--}}
    @yield('content')
</div>
@include('layouts.admin.sections.js')
</body>
</html>

<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>O'Live To Party Recipes</title>

    <!-- Bootstrap Core CSS -->
    <link href="http://netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css" rel="stylesheet" type="text/css">

    <!-- Fonts -->
    <link href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href='http://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>

    {{ HTML::style('css/grayscale.css') }}

{{--<link rel="stylesheet" href="css/grayscale.css"/>--}}
</head>

<body id="page-top" data-spy="scroll" data-target=".navbar-custom">
   <!-- nav -->
   @include('layouts.includes.nav')
   <!-- main view -->
   @yield('content')

   <footer>
    @include('layouts.includes.footer')
   </footer>

</body>
</html>
@extends('layouts.admin.dashboard')



@section('content')
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Recipes</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
{{ link_to_route('recipes.create', 'Add A New Recipe') }}

<table class="table table-hover">
 <thead>
 <tr>
 <th>Image</th>
 <th>Title</th>
 <th>Pairs</th>
 <th>Description</th>
 </tr>
 </thead>
 <tbody>

 @if(!empty($recipes))
 @foreach($recipes as $recipe)
  <tr>
 <td><img src="img/{{ $recipe->img }}" height="55" width="92" /></td>
 <td>{{ $recipe->title }}</td>
 <td>{{ $recipe->pairs }}</td>
 <td>{{ $recipe->description }}</td>
 <td>{{ link_to_route('recipes.show', 'Show', array($recipe->id)) }}</td>
 <td>{{ link_to_route('recipes.edit', 'Edit', array($recipe->id)) }}</td>
 <td>{{ link_to_route('recipeinstructions.show', 'Instructions/Ingredients', array($recipe->id)) }}</td>
 <td>{{ Form::open(array('method' => 'DELETE', 'route' => array('recipes.destroy', $recipe->id))) }}
     {{ Form::submit('Delete') }}
     {{ Form::close() }}
 </td>
  </tr>
 @endforeach
     @else
 <p>There are no recipes</p>
     @endif

 </tbody>
</table>
</div>

@stop
@extends('layouts.admin.dashboard')

@section('content')
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Recipes > Ingredients and Instructions</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>

        {{ link_to_route('recipes.index', 'Back') }}

        <div class="row">
            <div class="col-lg-12">
                <h1 style="color:#337AB7">{{ $recipes->title }}</h1>
                <br/>

                <h2>Instructions</h2>

                <div class="form-inline">
                    <div class="form-group">
                        {{ Form::open([ 'route' => 'recipeinstructions.store']) }}
                        {{ Form::hidden('id', $recipes->id) }}

                        {{ Form::textarea('instruction',null, array('class' => 'form-control', 'rows' => '3', 'cols' => '100', 'placeholder' => 'Add A New Instruction')) }}
                        {{ Form::submit('Add', array('class' => 'btn btn-primary', 'style' => 'padding:32px')) }}

                        {{ Form::close() }}
                    </div>
                </div>

                <br/>
                <hr/>


                <table class="table table-hover">

                    {{--instructions--}}
                    @foreach($recipes->instructions as $instruction)

                        <tr>
                            <td>{{ $instruction->instruction }}</td>

                            <td>{{ link_to_route('recipeinstructions.edit', 'Edit', $instruction->id) }}</td>
                            <td>{{ Form::open(array('method' => 'DELETE', 'route' => array('recipeinstructions.destroy', $instruction->id))) }}
                                {{ Form::submit('Delete') }}
                                {{ Form::close() }}
                            </td>
                        </tr>

                    @endforeach
                </table>
                <br/>

                <h2>Ingredients</h2>

                <div class="form-inline">
                    <div class="form-group">
                        {{ Form::open(['route' => 'recipeinstructions.store']) }}
                        {{ Form::hidden('id', $recipes->id) }}

                        {{ Form::textarea('ingredient',null, array('class' => 'form-control', 'rows' => '3', 'cols' => '100', 'placeholder' => 'Add An Instruction Here')) }}
                        {{ Form::submit('Add', array('class' => 'btn btn-primary', 'style' => 'padding:32px')) }}

                        {{ Form::close() }}

                    </div>
                </div>
                <br/>
                <hr/>

                <table class="table table-hover">
                    <tbody>
                {{--ingredients--}}
                @foreach($recipes->ingredients as $ingredients)
                    <tr>
                        <td>{{ $ingredients->ingredient }}</td>
                        <td>{{ link_to_route('recipeingredients.edit', 'Edit', $ingredients->id) }}</td>
                        <td>{{ Form::open(array('method' => 'DELETE', 'route' => array('recipeingredients.destroy', $ingredients->id))) }}
                            {{ Form::submit('Delete') }}
                            {{ Form::close() }}
                        </td>
                    </tr>

                @endforeach
                </table>

            </div>
        </div>
    </div>
@stop

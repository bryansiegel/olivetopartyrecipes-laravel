@extends('layouts.admin.dashboard')

@section('content')
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Instructions/Ingredients > Edit</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        {{ Form::model($recipe, array('method' => 'PATCH', 'route' =>
         array('recipeinstructions.update', $recipe->id))) }}

        {{ Form::hidden($recipe->id, 'id') }}


        <div class="form-group">
            {{ Form::label('instruction', 'Instruction:') }}
            {{ Form::text('instruction', $recipe->instruction, array('class' => 'form-control')) }}
        </div>


        {{ Form::submit('Update', array('class' => 'btn btn-info')) }}
        {{ link_to_route('recipeinstructions.show', 'Cancel', array($recipe->id)) }}
        {{ Form::close() }}
    </div>

@stop


@extends('layouts.admin.dashboard')

@section('content')
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Recipes > Show</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>


        {{ link_to_route('recipes.index', 'Back') }}
        <br/>

        <h3><u>Title</u></h3>
        {{ $recipes->title }}
        <h3><u>Pairs</u></h3>
        {{ $recipes->pairs }}
        <h3><u>Description</u></h3>
        {{ $recipes->description }}
        <h3><u>Image</u></h3>
        <img src='/img/{{ $recipes->img }}' alt="{{ $recipes->title }}" width="125" height="125"/>
        <hr/>
        <h2><u>Instructions</u></h2>
        @foreach($recipes->instructions as $instruction)
            {{ $instruction->instruction }}<br/>
            <hr/>
        @endforeach()
        <h2><u>Ingredients</u></h2>
        @foreach($recipes->ingredients as $ingredient)
            {{ $ingredient->ingredient }}<br/>
            <hr/>
        @endforeach()


    </div>
@stop

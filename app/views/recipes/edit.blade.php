@extends('layouts.admin.dashboard')

@section('content')
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Recipes > Edit</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        {{ Form::model($recipe, array('method' => 'PATCH', 'route' =>
         array('recipes.update', $recipe->id))) }}
           <div class="form-group">
               {{ Form::label('image', 'Image:') }}
               {{ Form::file('img') }}
           </div>

            <div class="form-group">
                {{ Form::label('title', 'Title:') }}
                {{ Form::text('title', null,array('class' => 'form-control')) }}
            </div>
            <div class="form-group">
                {{ Form::label('pairs', 'Pairs:') }}
                {{ Form::text('pairs', null, array('class' => 'form-control')) }}
            </div>
            <div class="form-group">
                {{ Form::label('description', 'Description:') }}
                {{ Form::textarea('description', null, array('class' => 'form-control', 'cols' => 5, 'rows' => 3)) }}
            </div>
                {{ Form::submit('Update', array('class' => 'btn btn-info')) }}
                {{ link_to_route('recipes.index', 'Cancel', array('class' => 'btn')) }}

        {{ Form::close() }}
    </div>

        @stop


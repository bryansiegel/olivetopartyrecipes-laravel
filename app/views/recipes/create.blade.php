@extends('layouts.admin.dashboard')

@section('content')
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Recipes > Create</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>


{{ Form::open(array('route' => 'recipes.store', 'files' => true)) }}
        <div class="form-group">
            {{ Form::label('title', 'Title:') }}
            {{ Form::text('title',null, array('class' => 'form-control', 'placeholder' => 'Title')) }}
        </div>

        <div class="form-group">
            {{ Form::label('pairs', 'Pairs:') }}
            {{ Form::text('pairs', null, array('class' => 'form-control', 'placeholder' => 'Pairs')) }}
        </div>

        <div class="form-group">
            {{ Form::label('description', 'Description:') }}
            {{ Form::textarea('description', null, array('class' => 'form-control', 'cols' => 5, 'rows' => 3, 'placeholder' => 'Description')) }}
        </div>
        <div class="form-group">
            {{ Form::label('img', 'Image') }}
            {{ Form::file('img') }}
        </div>


            {{ Form::submit('Submit', null, array('class' => 'btn btn-default')) }}



{{ Form::close() }}


</div>

@stop
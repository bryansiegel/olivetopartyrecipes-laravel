@extends('layouts.master')

@section('content')
   @include('layouts.includes.intro')
   @include('layouts.sections.about')
   @include('layouts.sections.recipes')
   @include('layouts.sections.modal')
@stop
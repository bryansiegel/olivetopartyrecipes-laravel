@extends('layouts.admin.login')

@section('content')

<div class="container">
<div class="row col-md-4">
<div class="well">

<div class="page-header">
<h1>Login</h1>
</div>

{{ Form::open(['url' => 'admin/login']) }}
<div class="form-group">
    {{ Form::label('username', 'Username')}}
    {{ Form::text('username', null, ['class' => 'form-control']) }}
</div>

<div class="form-group">
    {{ Form::label('password', 'Password') }}
    {{ Form::password('password',  ['class' => 'form-control']) }}
</div>

<div class="form-group">
{{ Form::submit('Login', ['class' => 'btn btn-primary']) }}
</div>

{{ Form::close() }}

</div>
</div>
</div><!--end container -->
@stop
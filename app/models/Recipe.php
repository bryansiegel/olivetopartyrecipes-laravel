<?php

class Recipe extends \Eloquent {

    protected $fillable = ['title', 'pairs', 'description', 'img'];

//    protected $table = "recipes";

    public static  $rules = array(
      'title' => 'required|min:2',
        'pairs' => 'required|min:1',
        'description' => 'required|min:1'
    );

   public function instructions()
   {
       return $this->hasMany('Instruction');
   }

    public function ingredients()
    {
        return $this->hasMany('Ingredient');
    }
}
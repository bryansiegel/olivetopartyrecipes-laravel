<?php

class Home extends \Eloquent {

    protected $fillable = ['title', 'pairs', 'description'];

    protected $table = "recipes";
}
<?php

class Instruction extends \Eloquent {
	protected $fillable = ['instruction', 'recipe_id'];

//    protected $table = "instructions";

    public static $rules = [
      'instructions' => 'required|min:2'
    ];

    public function recipe()
    {
        return $this->belongsTo('Recipe');
    }
}
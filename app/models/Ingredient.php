<?php

class Ingredient extends \Eloquent {
	protected $fillable = ['ingredient', 'recipe_id'];

//    protected $table = "ingredients";


    public static $rules = [
      'ingredients' => 'required|min:2'
    ];

    //recipes

    public function recipe()
    {
        return $this->belongsTo('Recipe');
    }

}